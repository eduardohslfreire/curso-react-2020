//Dispatcher -> Arquitetura Flux

import { useReducer } from 'react'

const STATE_INICIAL = {
    resultado: ''
}

const operaReducer = ( state = STATE_INICIAL, action ) => {
    switch( action.type ){
        case 'SUB':
        case 'SOMA':
            return {...state, resultado: action.payload }
        default:
            return state;    
    }
}

const useStore = () => useReducer(operaReducer, STATE_INICIAL)
export default useStore