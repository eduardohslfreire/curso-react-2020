import React, { useState, useEffect } from 'react';

import useStore from './somaReducer'

function ReducerHook() {
  
  const [ numero, setNumero ] = useState(1)
  const [ segundoNumero, setSegundoNumero ] = useState('')
  
  const [store, dispatch] = useStore()  

/*   const [ state, setState ] = useState({
    numero: 0,
    segundoNumero: 0,
    resultado: 0
  }); */

  const somar = () => {
    const numeroInt = parseInt(numero)
    const segNumeroInt = parseInt(segundoNumero)

    dispatch({
        type: 'SOMA',
        payload: numeroInt + segNumeroInt
    })

  }

  const subtrair = () => {
    const numeroInt = parseInt(numero)
    const segNumeroInt = parseInt(segundoNumero)

    dispatch({
        type: 'SUB',
        payload: numeroInt - segNumeroInt
    })

  }


 // useEffect(() => { //<--> DidMount
  //  console.log('variavel número:', numero)
  //},[]) //[] -> recebe variavel que deseja observar modificacoes



  return (
    <div >
      Numero 1:<br />
      <input type="text" value={numero} onChange={ (e) => setNumero(e.target.value)}/><br />
      Numero 2:<br />
      <input type="text" value={segundoNumero} onChange={ (e) => setSegundoNumero(e.target.value)}/><br />
      Resultado:<br />
      <button onClick={somar}>Somar</button>
      <button onClick={subtrair}>Subtrair</button><br />

      <input type="text" value={store.resultado} readOnly/><br />
    </div>
  );
}

export default ReducerHook;
