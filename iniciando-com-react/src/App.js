import React, { Fragment } from 'react';

/* function ComponeteeFuncional(){
  return (
    <h1>Hello</h1>
  )
} */


// function App (props){
//  Nao possui state, nem this, ciclo de vida, ...
// Metodos precisam ser declarados como const
// }

class App extends React.Component {
  
  //Valores que devem ser modificados
  state = {
    nome : "Eduardo Freire"
  }


  // Caso nao queira utilizar Arrow functions
  // constructor(){ 
  //   super();
  //   this.modificarNome = this.modificarNome.bind(this);
  // }
  // modificarNome(){}
  

  modificarNome = (event) => {
     this.setState({
       nome : event.target.value
     })
  }

  criarCombox = () => {
    const opcoes = [ "Fulano" , "Cicrano" ]
    const comboBoxOpcoes = opcoes.map(opcao => <option>{opcao}</option>)

    return(
      <select>{comboBoxOpcoes}</select>
    )
  
  }

  // componentDidMount(){
  // console.log('Executou o componete componentDidMount')
  // }

  render(){
   // console.log('Executou o componete render()')

    const MeuComboBox = () => this.criarCombox()

    return (
    <>{/*<React.Fragment>*/}
        <input type="text" value={this.state.nome} onChange={this.modificarNome}/>
        <h1>Hello {this.props.nome}, sua idade eh {this.props.idade} </h1>,
        {this.criarCombox()},
        <MeuComboBox />
    </>/*</React.Fragment>*/
     )
  }

  // render(){
  //   return (
  //   [
  //       <input type="text" value={this.state.nome} onChange={this.modificarNome}/>,
  //       <h1>Hello {this.state.nome} </h1>
  //   ]
  //    )
  // }
}

export default App;