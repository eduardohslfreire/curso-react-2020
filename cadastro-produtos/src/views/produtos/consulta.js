import React from 'react'

import Card from '../../components/card'
import ProdutosTable from './produtosTable'
import ProdutoService from '../../app/produtoService'
import { withRouter } from 'react-router-dom'

class Consulta extends React.Component {

    state = {
        produtos: []
    }

    constructor(){
        super()
        this.service = new ProdutoService()
    }

    componentDidMount(){
        const produtos = this.service.obterProdutos();
        this.setState({ produtos: produtos }) //this.setState({ produtos }) 
    }

    preparaEditar = (sku) => {
        this.props.history.push(`/cadastro-produtos/${sku}`) //history vem do withRouter
    }

    deletar = (sku) => {
        const produtos = this.service.deletar(sku)
        this.setState({produtos})
    }


    render() {
        return (
            <Card header="Consulta produtos">
                     <ProdutosTable produtos={this.state.produtos}
                     actionEdit={this.preparaEditar}
                     actionDelete={this.deletar} />
            </Card>
        )
    }
}

export default withRouter(Consulta)